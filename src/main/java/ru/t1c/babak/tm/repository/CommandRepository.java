package ru.t1c.babak.tm.repository;

import ru.t1c.babak.tm.api.ICommandRepository;
import ru.t1c.babak.tm.constant.ArgumentConst;
import ru.t1c.babak.tm.constant.TerminalConst;
import ru.t1c.babak.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info.");

    private static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show application version.");

    private static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show terminal commands.");

    private static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show system info.");

    private static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application.");

    private static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Show application commands.");

    private static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Show application arguments.");

    private final Command[] terminalCommands = new Command[]{
            ABOUT,
            VERSION,
            HELP,
            INFO,
            COMMANDS,
            ARGUMENTS,
            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return terminalCommands;
    }

}
