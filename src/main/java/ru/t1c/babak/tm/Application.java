package ru.t1c.babak.tm;

import ru.t1c.babak.tm.api.ICommandRepository;
import ru.t1c.babak.tm.constant.ArgumentConst;
import ru.t1c.babak.tm.constant.TerminalConst;
import ru.t1c.babak.tm.model.Command;
import ru.t1c.babak.tm.repository.CommandRepository;
import ru.t1c.babak.tm.util.InformationData;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;

/**
 * Artem Babak
 */
public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(final String[] args) {
        if (runArgument(args)) System.exit(0);
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND: ");
            final String command = scanner.nextLine();
            runCommand(command);
        }
    }

    public static void runArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            default:
                showErrorArgument(arg);
                break;
        }
    }

    public static void runCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.COMMANDS:
                showCommands();
                break;
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.INFO:
                showSystemInfo();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                showErrorCommand(command);
                break;
        }
    }

    public static boolean runArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        runArgument(arg);
        return true;
    }

    public static void showWelcome() {
        System.out.println("** WELCOME TO TASK-MANAGER! **");
    }

    public static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = runtime.freeMemory();
        final InformationData freeMemoryData = new InformationData(freeMemory);
        System.out.println("Free memory: " + freeMemoryData);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final InformationData maxMemoryData = new InformationData(maxMemory);
        System.out.println("Maximum memory: " +
                (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryData));
        final long totalMemory = runtime.totalMemory();
        final InformationData totalMemoryData = new InformationData(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryData);
        final long usedMemory = totalMemory - freeMemory;
        final InformationData usedMemoryData = new InformationData(usedMemory);
        System.out.println("Used memory in JVM: " + usedMemoryData);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Babak Artem");
        System.out.println("E-mail: ababak@t1-consulting.ru");
    }

    /**
     * Getting version from pom.xml ${project.version}
     */
    public static void showVersion() {
        System.out.println("[VERSION]");
        final Properties properties = new Properties();
        final InputStream inputStream = Application.class
                .getClassLoader()
                .getResourceAsStream("project.properties");
        try {
            properties.load(inputStream);
            System.out.println(properties.getProperty("version"));
            if (inputStream != null) inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void close() {
        System.exit(0);
    }

    public static void showCommands() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name + " : " + command.getDescription());
        }
    }

    public static void showArguments() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument + " : " + command.getDescription());
        }
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands)
            System.out.println(command);
    }

    public static void showErrorArgument(final String arg) {
        System.err.printf("Error! Unknown argument: `%s`", arg);
    }

    public static void showErrorCommand(final String arg) {
        System.err.printf("Error! Unknown command: `%s` \n", arg);
    }

}
